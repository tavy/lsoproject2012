#include "lib/const.h"
#include "lib/types11.h"
#include "lib/uMPStypes.h"
#include "lib/libumps.e"
#include "lib/pcb.e"
#include "lib/asl.e"
#include "lib/utils.e"
#include "lib/scheduler.e"

int cause,intcounter=0, intcounter2=0;
U32 getEpc=0,oldEpc=0;
int run_null=9;
extern U32 test3;
extern pcb_t *running_process[MAX_CPU];
extern state_t* areasPointers[MAX_CPU];
extern int init_proc_time[MAX_CPU];
extern struct list_head ready_queue;
extern unsigned int mutex_rq, run_mutex[MAX_CPU], mutex_active_proc, mutex_store_status, mutex_semdh;
extern unsigned int active_proc;
extern unsigned int dev_semaphores_keys[6][8];
state_t* old_states[MAX_CPU];
U32 epcs[MAX_CPU];
state_t* prova;

int store_device_status =NULL;

void pltHandler(){
	/*ack*/
	setTIMER(SCHED_TIME_SLICE);
}

void ipiHandler(){
	char mess = ((*(int*)IPI_IN_ADDR)<<24)>>24;
	/*ack*/
	*(int*)IPI_IN_ADDR = 1;
	/*se il messaggio è 't' cioè termina processo*/
	if(mess=='t'){
		run_null=5;
		SYSCALL(TERMINATEPROCESS,0,0,0);
	}
}
void timerHandler(){
	/*sblocca i processi che aspettano*/
	SYSCALL(VERHOGEN,CLOCK_TIMER,0,0);
	/*ack*/
	SET_IT(SCHED_PSEUDO_CLOCK);
}
void terminalHandler(){
	int bit_map = *(((int *)INIT_INTER_DEV_BIT_MAP)+4);/*4 nell'intervallo 0-4 cioe' 7 nell'intervallo 3-7 */
	int i,bits;
	termreg_t* base;
	for(i=0,bits=1;i<8;i++,bits<<=1){
		int sub_device=0,loc_store=0;
		if(bit_map & bits){
			/*Formula: devAddrBase = 0x1000.0050 +((IntlineNo - 3) * 0x80) + (DevNo * 0x10)*/
			while(!CAS(&run_mutex[0],CFREE,CBUSY));
			base=(termreg_t*)(DEV_REGS_START+(4*0x80)+(i*0x10));
			/*Questo if serve a capire quale dei due subdevice ha causato l'interrupt, viene gestito prima il trasmettitore*/	
			/*Controlliamo lo status device del trasmettitore*/
			if((base->transm_status & 0xFF) == 5){
				loc_store = (int)base->transm_status;
				/*ACK to the trasmitter*/
				base->transm_command=1;
			/*Controlliamo lo status device del ricevittore*/
			}else if((base->recv_status & 0xFF) == 5){
				/*ACK to the trasmitter*/
				sub_device=1;
				loc_store = (int)base->recv_status;
				/*ACK to the receiver*/
				base->recv_command=1;
			}
			if((loc_store & 0xFF)==5){			
				while(!CAS(&mutex_semdh,CFREE,CBUSY));
				if(!headBlocked(getSemd(dev_semaphores_keys[(4+sub_device)][i]))){
					while(!CAS(&mutex_store_status,CFREE,CBUSY));
					store_device_status = loc_store;
					CAS(&mutex_store_status,CBUSY,CFREE);
				}
				CAS(&mutex_semdh,CBUSY,CFREE);
				run_null = loc_store;
				SYSCALL(VERHOGEN,dev_semaphores_keys[(4+sub_device)][i],0,0);
			}else{
				run_null = loc_store;
			}
			CAS(&run_mutex[0],CBUSY,CFREE);
			break;
		}
	}
}
void diskHandler(){
	int bit_map = *((int *)INIT_INTER_DEV_BIT_MAP);/* non aggiungiamo niente perché i dischi sono i primi quindi punta già alla posizione giusta */
	int i,bits;
	dtpreg_t* base;
	for(i=0,bits=1;i<8;i++,bits<<=1){
		if(bit_map & bits){
			/*Formula: devAddrBase = 0x1000.0050 +((IntlineNo - 3) * 0x80) + (DevNo * 0x10)*/
			base = (dtpreg_t*) DEV_REGS_START+(i*0x10);
			/*ACK to the disk*/
			base->command=1;
			SYSCALL(VERHOGEN,dev_semaphores_keys[0][i],0,0);
			break;
		}
	}
}
void tapeHandler(){
	int bit_map = (int)(((int *)INIT_INTER_DEV_BIT_MAP)+1);/*1 nell'intervallo 0-4 cioe' 4 nell'intervallo 3-7 */
	int i,bits;
	dtpreg_t* base;
	for(i=0,bits=1;i<8;i++,bits<<=1){
		if(bit_map & bits){
			/*Formula: devAddrBase = 0x1000.0050 +((IntlineNo - 3) * 0x80) + (DevNo * 0x10)*/
			base = (dtpreg_t*) DEV_REGS_START+(i*0x10);
			/*ACK to the disk*/
			base->command=1;
			SYSCALL(VERHOGEN,dev_semaphores_keys[1][i],0,0);
			break;
		}
	}

}
void printerHandler(){
	int bit_map = (int)(((int *)INIT_INTER_DEV_BIT_MAP)+3);/*3 nell'intervallo 0-4 cioe' 5 nell'intervallo 3-7 */
	int i,bits;
	dtpreg_t* base;
	for(i=0,bits=1;i<8;i++,bits<<=1){
		if(bit_map & bits){
			/*Formula: devAddrBase = 0x1000.0050 +((IntlineNo - 3) * 0x80) + (DevNo * 0x10)*/
			base = (dtpreg_t*) DEV_REGS_START+(i*0x10);
			/*ACK to the disk*/
			base->command=1;
			SYSCALL(VERHOGEN,dev_semaphores_keys[3][i],0,0);
			break;
		}
	}


}
void networkHandler(){
	int bit_map = (int)(((int *)INIT_INTER_DEV_BIT_MAP)+2);/*2 nell'intervallo 0-4 cioe' 6 nell'intervallo 3-7 */
	int i,bits;
	dtpreg_t* base;
	for(i=0,bits=1;i<8;i++,bits<<=1){
		if(bit_map & bits){
			/*Formula: devAddrBase = 0x1000.0050 +((IntlineNo - 3) * 0x80) + (DevNo * 0x10)*/
			base = (dtpreg_t*) DEV_REGS_START+(i*0x10);
			/*ACK to the disk*/
			base->command=1;
			SYSCALL(VERHOGEN,dev_semaphores_keys[2][i],0,0);
			break;
		}
	}


}

void saveProcess(int cpu_id,int time){
	if(running_process[cpu_id]!=NULL){
		/*Usare stateCpy invece di memcpy fa risparmiare circa 2200 cicli cpu*/
		/*while(!CAS(&run_mutex[cpu_id],CFREE,CBUSY));*/
		while(!CAS(&mutex_rq,CFREE,CBUSY));
		stateCpy(&running_process[cpu_id]->p_s,areasPointers[cpu_id]);
		running_process[cpu_id]->cpu_time+=(time-init_proc_time[cpu_id]);
		insertProcQ(&ready_queue,running_process[cpu_id]);		
		CAS(&mutex_rq,CBUSY,CFREE);
		while(!CAS(&mutex_active_proc,CFREE,CBUSY));
		active_proc--;
		CAS(&mutex_active_proc,CBUSY,CFREE);
		running_process[cpu_id]=NULL;
/*		CAS(&run_mutex[cpu_id],CBUSY,CFREE);*/
	}
}

void intHandler(){
	int int_time = GET_TODLOW;
	int cpu_id = getPRID();
	int is_ipi = 0;
	/*epcs[cpu_id]=getEPC();*/
	areasPointers[cpu_id]->pc_epc=getEPC();
	cause = getCAUSE();	
	if(CAUSE_IP_GET(cause,INT_IPI)){
		ipiHandler();
		is_ipi=1;
	}
       	if(!is_ipi)	
		saveProcess(cpu_id,int_time);

	if(CAUSE_IP_GET(cause,INT_PLT)){
		pltHandler();
	}
	if(CAUSE_IP_GET(cause,INT_TIMER)){
		timerHandler();
	}
	if(CAUSE_IP_GET(cause,INT_DISK)){
		diskHandler();
	}
	if(CAUSE_IP_GET(cause,INT_TAPE)){
		tapeHandler();
	}
	if(CAUSE_IP_GET(cause,INT_UNUSED)){
		networkHandler();
	}
	if(CAUSE_IP_GET(cause,INT_PRINTER)){
		printerHandler();
	}
	if(CAUSE_IP_GET(cause,INT_TERMINAL)){
		terminalHandler();
	}
	

	scheduler();
}



