#include "lib/const.h"
#include "lib/types11.h"
#include "lib/uMPStypes.h"
#include "lib/pcb.e"
#include "lib/utils.e"
#include "lib/libumps.e"

extern unsigned int proc_counter,soft_block_counter,active_proc;
extern struct list_head ready_queue;
extern unsigned int mutex_rq, run_mutex[MAX_CPU], mutex_proc_c, mutex_active_proc;
pcb_t *running_process[MAX_CPU];
int init_proc_time[MAX_CPU];

int var_sch = 0,empty;
int s=0;

int status=0;

int priors[MAX_CPU];

state_t pstate;
void scheduler(void){
	int cpu_id = getPRID();
/*	setTIMER(SCHED_TIME_SLICE);*/
	/*var_sch = (status & STATUS_PLT_ENABLED)== STATUS_PLT_ENABLED;*/
	/*forse inutile-> if(!((status & STATUS_PLT_ENABLED) == STATUS_PLT_ENABLED)){*/

/*	status = getSTATUS();*/
/*	s = 0 |  (STATUS_IEc | STATUS_INT_UNMASKED | STATUS_PLT_ENABLED);
	setSTATUS(s);*/
	while(!CAS(&mutex_rq,CFREE,CBUSY));
	if(!emptyProcQ(&ready_queue)){
		/*settare status per lo scheduler*/
/*		status=mutex_rq;*/
	/*	s=running_process[cpu_id]->priority;*/
	/*	while(!CAS(&run_mutex[cpu_id],CFREE,CBUSY));*/
		running_process[cpu_id]=removeProcQ(&ready_queue);
		/*Aggiorna il TRP per l'Interrupt Routing Dinamico*/
		*(int*)TRP_ADDR=(15-running_process[cpu_id]->priority);
/*		CAS(&run_mutex[cpu_id],CBUSY,CFREE);*/
	/*	var_sch = running_process[cpu_id]->priority;*/
		CAS(&mutex_rq,CBUSY,CFREE);
		/*status=mutex_rq;*/
		/*pstate = running_process[cpu_id]->p_s;*/
		/*priors[cpu_id]=16-running_process[cpu_id]->priority;*/
		status=(int)*(int*)TRP_ADDR;
		while(!CAS(&mutex_active_proc,CFREE,CBUSY));
		active_proc++;
		CAS(&mutex_active_proc,CBUSY,CFREE);
		setTIMER(SCHED_TIME_SLICE);
		init_proc_time[cpu_id] = GET_TODLOW;
		LDST(&running_process[cpu_id]->p_s);		
	}else{
		/*Deadlock detection*/
		/*METTERE MUTEX ALLE VARIABILI CONDIVISE!!*/
		while(!CAS(&mutex_proc_c,CFREE,CBUSY));
		if(!proc_counter)
			HALT();
		else if(proc_counter>0 && soft_block_counter==0 && active_proc==0)
			PANIC();
		else if(proc_counter==0 && soft_block_counter>0){
			CAS(&mutex_proc_c,CBUSY,CFREE);
			WAIT();
		}else{
			CAS(&mutex_proc_c,CBUSY,CFREE);
			CAS(&mutex_rq,CBUSY,CFREE);
			/*	while(!CAS(&run_mutex[cpu_id],CFREE,CBUSY));*/
			running_process[cpu_id]=NULL;
			/*		CAS(&run_mutex[cpu_id],CBUSY,CFREE);*/
			/*In caso che non ci sia nessun processo da eseguire, assegniamo la priorità minore al trp del processore in modo che se c'e' un interrupt da gestire l'interrupt router lo assegnerà a questo processore.*/
			*(int*)TRP_ADDR=15;
			setTIMER(SCHED_FREE_TIME);
			setSTATUS(STATUS_PLT_ENABLED | STATUS_INT_UNMASKED | STATUS_IEc);
			while(1);
		}
	}
	
	
	/*	}*/

}
