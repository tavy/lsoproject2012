#include "lib/const.h"
#include "lib/types11.h"
#include "lib/uMPStypes.h"
#include "lib/libumps.e"
#include "lib/pcb.e"
#include "lib/utils.e"

extern pcb_t* running_process[MAX_CPU];
extern state_t* areasPointers[MAX_CPU];

int trapCause=0, trapExc=0;
void tlbHandler(){
	int cpu_id = getPRID();
	if(running_process[cpu_id]->own_tlb!=NULL){
		stateCpy(running_process[cpu_id]->own_olds_tlb,(areasPointers[cpu_id])+2);    
	/*	STST(running_process[cpu_id]->own_olds_tlb);	*/
		LDST(running_process[cpu_id]->own_tlb);
	}else
		SYSCALL(TERMINATEPROCESS,0,0,0);
}

void trapHandler(){
/*	trapCause = getCAUSE();*/
/*	trapExc = CAUSE_EXCCODE_GET(trapCause);	*/
	int cpu_id = getPRID();
	if(running_process[cpu_id]->own_trap!=NULL){
		stateCpy(running_process[cpu_id]->own_olds_trap,(areasPointers[cpu_id])+4);    
/*		STST(running_process[cpu_id]->own_olds_trap);	*/
		LDST(running_process[cpu_id]->own_trap);
	}else
		SYSCALL(TERMINATEPROCESS,0,0,0);
}
