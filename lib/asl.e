#ifndef ASL_E
#define ASL_E

#include "const.h"
#include "types11.h"
#include "listx.h"

/* ASL handling functions */
semd_t* getSemd(int key);
void initASL();

semd_t* allocSemd(int key);
int insertBlocked(semd_t* semd_found,pcb_t* p);
pcb_t* removeBlocked(semd_t* semd_found);
pcb_t* outBlocked(pcb_t *p);
pcb_t* headBlocked(semd_t* semd_found);
void outChildBlocked(pcb_t *p);

#endif
