void syscallHandler(void);
void *memcpy(void *dest,const void *src, size_t n);
void myPrint(char *msg);
void stateCpy(state_t* dest, state_t* src);
void stateCpy2(state_t* dest, state_t* src);
void pcbCpy(pcb_t* dest, pcb_t* src);
void pcbSetZero(pcb_t* p);
