#ifndef _TYPES11_H
#define _TYPES11_H

#include "uMPStypes.h"
#include "listx.h"

/* Process Control Block (PCB) data structure */
typedef struct pcb_t {
	/*process queue fields */
	struct list_head	p_next;

	/*process tree fields */
	struct pcb_t		*p_parent;
	struct list_head	p_child, p_sib;
	/* processor state, etc */
	state_t       		p_s;   

	/* process priority */
	int priority;
	
	/* key of the semaphore on which the process is eventually blocked */
	int p_semkey;

	/*aggiunti da noi*/
	U32 cpu_time;
	/*migliorabile fare una cosa dinamica*/
	state_t* own_trap;
	state_t* own_olds_trap;
	state_t* own_tlb;
	state_t* own_olds_tlb;
	state_t* own_sys;
	state_t* own_olds_sys;

} pcb_t;



/* Semaphore Descriptor (SEMD) data structure*/
typedef struct semd_t {
	struct list_head	s_next;
	
	/* Semaphore value*/
	int					s_value;
	
	/* Semaphore key*/
	int					s_key;
	
	/* Queue of PCBs blocked on the semaphore*/
	struct list_head	s_procQ;
} semd_t;



#endif
