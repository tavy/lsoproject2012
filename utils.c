#include "lib/const.h"
#include "lib/types11.h"
#include "lib/uMPStypes.h"
#include "lib/scheduler.e"
#include "lib/pcb.e"
#include "lib/asl.e"
#include "lib/utils.e"
#include "lib/libumps.e"


typedef unsigned int devregtr;
extern void* prova;
extern state_t* areasPointers[MAX_CPU];
extern pcb_t* running_process[MAX_CPU];
extern int init_proc_time[MAX_CPU];
extern unsigned int proc_counter,active_proc,soft_block_counter;
extern unsigned int mutex_rq, run_mutex[MAX_CPU], mutex_proc_c, mutex_semdh, mutex_pcbh, mutex_active_proc, mutex_soft_counter, mutex_store_status;
extern unsigned int sem_mutex[MAXSEM];
extern unsigned int dev_semaphores_keys[6][8];
extern struct list_head ready_queue;
extern int store_device_status;
/*extern int counters[MAX_CPU];*/
extern int trapExc;
/*unsigned int mutex_sem = CFREE; 
unsigned int mutex_sem2 = CFREE; */
semd_t sem_found;
semd_t* sem_found2;
int test4=0;
state_t old_s;	

pcb_t ppppp;

int syscallCreateProcess(state_t* statep, int priority, int cpu_id){
	while(!CAS(&mutex_pcbh,CFREE,CBUSY));
	pcb_t* new_process=allocPcb();
	CAS(&mutex_pcbh,CBUSY,CFREE);
	if(new_process==NULL)
		return -1;
/*	memcpy(&new_process->p_s,statep,sizeof(state_t));*/
	stateCpy(&new_process->p_s,statep);
	new_process->priority = priority;
	insertChild(running_process[cpu_id],new_process);
	while(!CAS(&mutex_rq,CFREE,CBUSY));
	insertProcQ(&ready_queue,new_process);
	CAS(&mutex_rq,CBUSY,CFREE);
	while(!CAS(&mutex_proc_c,CFREE,CBUSY));
	proc_counter++;
	CAS(&mutex_proc_c,CBUSY,CFREE);
	return 0;
}
int syscallCreateBrother(state_t* statep, int priority, int cpu_id){
	while(!CAS(&mutex_pcbh,CFREE,CBUSY));
	pcb_t* new_process=allocPcb();
	CAS(&mutex_pcbh,CBUSY,CFREE);
	if((new_process==NULL) || (running_process[cpu_id]->p_parent==NULL))
		return -1;
	/*memcpy(&new_process->p_s,statep,sizeof(state_t));*/
	stateCpy(&new_process->p_s,statep);
	new_process->priority = priority;
	insertChild(running_process[cpu_id]->p_parent,new_process);
	while(!CAS(&mutex_rq,CFREE,CBUSY));
	insertProcQ(&ready_queue,new_process);
	CAS(&mutex_rq,CBUSY,CFREE);
	while(!CAS(&mutex_proc_c,CFREE,CBUSY));
	proc_counter++;
	CAS(&mutex_proc_c,CBUSY,CFREE);
	return 0;
}

void TerminateProcess(pcb_t* p, int cpu_id){
	int i,bits=0,is_active=0;
	pcb_t* child;
	for(i=0;i<25;i++);
	if(p!=NULL){
		while(!CAS(&mutex_pcbh,CFREE,CBUSY));
		child=removeChild(p);
		CAS(&mutex_pcbh,CBUSY,CFREE);
		while(child!=NULL){
			TerminateProcess(child,cpu_id);
			while(!CAS(&mutex_pcbh,CFREE,CBUSY));
			child=removeChild(p);
			CAS(&mutex_pcbh,CBUSY,CFREE);
		}	
		for(i=0,bits=1;i<MAX_CPU;i++,bits<<=1){
			/*	while(!CAS(&run_mutex[i],CFREE,CBUSY));*/
			if((i!=cpu_id) && (running_process[i]==p)){
				is_active=1;
				break;
			}
			/*		CAS(&run_mutex[i],CBUSY,CFREE);
					if(is_active)
					break;*/
		}
		/*	trapExc=bits;*/
		if(is_active){
			trapExc=bits;
			*(int *)IPI_OUT_ADDR=(bits<<8)|'t';	

		}else{
			/*Eliminamo il processo dalla coda dei processi ready*/	
			while(!CAS(&mutex_rq,CFREE,CBUSY));
			outProcQ(&ready_queue,p);
			CAS(&mutex_rq,CBUSY,CFREE);
			while(!CAS(&mutex_semdh,CFREE,CBUSY));
			if(outBlocked(p)==p){
				while(!CAS(&mutex_soft_counter,CFREE,CBUSY));
				soft_block_counter--;
				CAS(&mutex_soft_counter,CBUSY,CFREE);
			}
			CAS(&mutex_semdh,CBUSY,CFREE);
			/*Liberiamo le risorse*/
			while(!CAS(&mutex_pcbh,CFREE,CBUSY));
			outChild(p);
			freePcb(p);
			CAS(&mutex_pcbh,CBUSY,CFREE);
			while(!CAS(&mutex_proc_c,CFREE,CBUSY));
			proc_counter--;
			CAS(&mutex_proc_c,CBUSY,CFREE);
			if(running_process[cpu_id]==p){
				while(!CAS(&mutex_active_proc,CFREE,CBUSY));
				active_proc--;
				CAS(&mutex_active_proc,CBUSY,CFREE);
			}
		}
	}
}

void syscallTerminateProcess(int cpu_id){
	TerminateProcess(running_process[cpu_id],cpu_id);
}

void syscallVerhogen(int semkey){
	pcb_t* unblocked;
	while(!CAS(&sem_mutex[semkey],CFREE,CBUSY));
/*	trapExc = semkey;*/
	while(!CAS(&mutex_semdh,CFREE,CBUSY));
	semd_t* sem=(semd_t*)getSemd(semkey);
	if(!sem){
		sem=(semd_t*)allocSemd(semkey);
	}
	/*	sem_found2 = sem;*/
	/*	if(sem->s_value<=0){*/
	if(headBlocked(sem)){
	/*	trapExc = 7;*/
		if(semkey==CLOCK_TIMER){
			while((unblocked=removeBlocked(sem))!=NULL){
			/*	trapExc = 1;*/
				while(!CAS(&mutex_rq,CFREE,CBUSY));
				insertProcQ(&ready_queue,unblocked);
				CAS(&mutex_rq,CBUSY,CFREE);
				while(!CAS(&mutex_soft_counter,CFREE,CBUSY));
				soft_block_counter--;
				CAS(&mutex_soft_counter,CBUSY,CFREE);
			}
		}else{
			if((unblocked=removeBlocked(sem))!=NULL){
				while(!CAS(&mutex_rq,CFREE,CBUSY));
				insertProcQ(&ready_queue,unblocked);
				CAS(&mutex_rq,CBUSY,CFREE);
				while(!CAS(&mutex_soft_counter,CFREE,CBUSY));
				soft_block_counter--;
				CAS(&mutex_soft_counter,CBUSY,CFREE);
			}
		}
	}else
		sem->s_value++;
	/*	}else{
		if(semkey==CLOCK_TIMER)
		sem->s_value=0;
		}*/
	CAS(&mutex_semdh,CBUSY,CFREE);
	CAS(&sem_mutex[semkey],CBUSY,CFREE);
/*	trapExc= 9;*/
}
void syscallPasseren(int semkey,int cpu_id,int* flag_scheduler){
/*	trapExc = semkey;*/
	while(!CAS(&sem_mutex[semkey],CFREE,CBUSY));
	while(!CAS(&mutex_semdh,CFREE,CBUSY));
	semd_t* sem=getSemd(semkey);
       
	if(sem==NULL)
		sem=allocSemd(semkey);
	
	if(sem->s_value>0){
		sem->s_value--;		
		CAS(&mutex_semdh,CBUSY,CFREE);
	}else{
	/*	trapExc = semkey;*/
		CAS(&mutex_semdh,CBUSY,CFREE);
		stateCpy(&running_process[cpu_id]->p_s,(areasPointers[cpu_id])+6);
		running_process[cpu_id]->p_s.pc_epc+=WORD_SIZE;
		insertBlocked(sem,running_process[cpu_id]);
		*flag_scheduler=1;
		while(!CAS(&mutex_soft_counter,CFREE,CBUSY));
		soft_block_counter++;
		CAS(&mutex_soft_counter,CBUSY,CFREE);
		while(!CAS(&mutex_active_proc,CFREE,CBUSY));
		active_proc--;
		CAS(&mutex_active_proc,CBUSY,CFREE);
	}
	CAS(&sem_mutex[semkey],CBUSY,CFREE);
}

int syscallGetCpuTime(int cpu_id){
	return running_process[cpu_id]->cpu_time;
}

void syscallWaitClock(int cpu_id, int *flag_scheduler){
	syscallPasseren(CLOCK_TIMER,cpu_id,flag_scheduler);
}

int syscallWaitIo(int intno, int dnum, int waitForTermRead,int cpu_id, int *flag_scheduler){
	int semkey;
	int base;
	int dev_stat;
/*	stateCpy(&old_s,(areasPointers[cpu_id])+6);*/
	/*SYSCALL(PASSEREN,semkey,0,0);*/
	while(!CAS(&run_mutex[0],CFREE,CBUSY));
	while(!CAS(&mutex_store_status,CFREE,CBUSY));
	if(store_device_status){
		dev_stat = store_device_status;
		store_device_status = NULL;
		CAS(&mutex_store_status,CBUSY,CFREE);
	}else{
		CAS(&mutex_store_status,CBUSY,CFREE);
		base = DEV_REGS_START+((intno-3)*0x80)+(dnum*0x10);
		if(intno==7){
			semkey = dev_semaphores_keys[intno-3-waitForTermRead][dnum];
			if(waitForTermRead==1)
				dev_stat = *(short int*)base;	
			else{
				dev_stat = *(short int*)(base+0x8);
			}
		}else{
			semkey = dev_semaphores_keys[intno-3][dnum];
			dev_stat = *(short int*)base;	
		}
	/*	trapExc = *(short int*)(base+0x8);*/
		((areasPointers[cpu_id])+6)->reg_v0 = dev_stat; 
		syscallPasseren(semkey,cpu_id,flag_scheduler);
	}
	CAS(&run_mutex[0],CBUSY,CFREE);
	return dev_stat;
}

void syscallSpecPrgVec(state_t* oldp,state_t* newp,int cpu_id, int* flag_scheduler){
	if(running_process[cpu_id]->own_trap==NULL){
		running_process[cpu_id]->own_trap=newp;
		running_process[cpu_id]->own_olds_trap=oldp;
	}else{
		syscallTerminateProcess(cpu_id);
		*flag_scheduler=1;
	}

}

void syscallSpecTlbVec(state_t* oldp,state_t* newp,int cpu_id, int* flag_scheduler){
	if(running_process[cpu_id]->own_tlb==NULL){
		running_process[cpu_id]->own_tlb=newp;
		running_process[cpu_id]->own_olds_tlb=oldp;
	}else{
		syscallTerminateProcess(cpu_id);
		*flag_scheduler=1;
	}
}
void syscallSpecSysVec(state_t* oldp,state_t* newp,int cpu_id, int* flag_scheduler){
	if(running_process[cpu_id]->own_sys==NULL){
		running_process[cpu_id]->own_sys=newp;
		running_process[cpu_id]->own_olds_sys=oldp;
	}else{
		syscallTerminateProcess(cpu_id);
		*flag_scheduler=1;
	}
}


void stateCpy(state_t* dest,state_t* src){
	int i;
	dest->entry_hi = src->entry_hi;
	dest->cause = src->cause;
	dest->status = src->status;
	dest->pc_epc = src->pc_epc;
	dest->hi = src->hi;
	dest->lo = src->lo;
	for(i=0;i<29;i++){
		dest->gpr[i]=src->gpr[i];
	}
}

void pcbSetZero(pcb_t* p){
	int i;
	p->p_next.next=0;
	p->p_next.prev=0;
	p->p_parent=0;
	p->p_child.next=0;
	p->p_child.prev=0;
	p->p_sib.next=0;
	p->p_sib.prev=0;
	p->priority=0;
	p->p_semkey=0;
	p->cpu_time=0;
	p->own_trap=0;
	p->own_olds_trap=0;
	p->own_tlb=0;
	p->own_olds_tlb=0;
	p->own_sys=0;
	p->own_olds_sys=0;

	p->p_s.entry_hi = 0;
	p->p_s.cause = 0;
	p->p_s.status = 0;
	p->p_s.pc_epc = 0;
	p->p_s.hi = 0;
	p->p_s.lo = 0;
	for(i=0;i<29;i++){
		p->p_s.gpr[i]=0;
	}


}

void syscallHandler(){
	int cpu_id = getPRID();
	if(running_process[cpu_id]!=NULL){
		running_process[cpu_id]->cpu_time+=(GET_TODLOW-init_proc_time[cpu_id]);
		init_proc_time[cpu_id]= GET_TODLOW;
	}
	int exc_code = CAUSE_EXCCODE_GET(getCAUSE());
	int retval=-2;
	int flag_scheduler=0;
	state_t* stat = (areasPointers[cpu_id])+6;
	int sysid = stat->reg_a0;
	if((stat->status <<28)>>31 ==0){
		if(exc_code==8){/*syscall*/
			switch(sysid){
				case 1:
					retval = syscallCreateProcess((state_t*)stat->reg_a1,stat->reg_a2,cpu_id);
					break;	
				case 2:
					retval = syscallCreateBrother((state_t*)stat->reg_a1,stat->reg_a2, cpu_id);
					break;	
				case 3:
					syscallTerminateProcess(cpu_id);
					break;	
				case 4:
					syscallVerhogen(stat->reg_a1);
					break;	
				case 5:
					syscallPasseren(stat->reg_a1,cpu_id,&flag_scheduler);
					break;	
				case 6:
					retval = syscallGetCpuTime(cpu_id);
					break;	
				case 7:
					syscallWaitClock(cpu_id,&flag_scheduler);
					break;	
				case 8:
					retval = syscallWaitIo(stat->reg_a1,stat->reg_a2,stat->reg_a3,cpu_id,&flag_scheduler);
					break;	
				case 9:
					syscallSpecPrgVec((state_t*)stat->reg_a1,(state_t*)stat->reg_a2,cpu_id,&flag_scheduler);
					break;	
				case 10:
					syscallSpecTlbVec((state_t*)stat->reg_a1,(state_t*)stat->reg_a2,cpu_id, &flag_scheduler);
					break;	
				case 11:
					syscallSpecSysVec((state_t*)stat->reg_a1,(state_t*)stat->reg_a2,cpu_id, &flag_scheduler);
					break;
				default:
					retval=-3;/*flag per vedere se il processo  ha specificato il suo SYS/BP handler*/

			}
			/*trapExc = retval;*/

		}else if(exc_code==9){/*breakpoint*/
			retval=-3;/*flag per vedere se il processo  ha specificato il suo SYS/BP handler*/
		}
	}else{
		retval = -3;
		test4=7;	
	}
	/*se c'e' stato un BrekPoint o se l'id della syscall e' maggiore di 11
	 * controlliamo se il processo ha definito il suo SYS/BP handler altrimenti lo terminiamo*/
	if(retval==-3){
	       	if(running_process[cpu_id]->own_sys!=NULL){
			stateCpy(running_process[cpu_id]->own_olds_sys,stat);    
			LDST(running_process[cpu_id]->own_sys);
		}else{
			sysid=3;		
			syscallTerminateProcess(cpu_id);
		}
	}
	
	if(flag_scheduler==1 || sysid==3)
		scheduler();
	else
		stat->pc_epc = stat->pc_epc+WORD_SIZE;
	if(retval!=-2)
		stat->reg_v0 = retval;
	LDST(stat);
}


void myPrint(char *msg){
	char* s = msg;
	devregtr* base = (devregtr *)0x10000250;
	devregtr status;
	SYSCALL(PASSEREN, 0, 0, 0);
	while (*s != '\0') {
		*(base + 3) = 2 | (((devregtr) *s) << 8);
		status = SYSCALL(WAITIO, INT_TERMINAL, 0, FALSE);
		test4=status;	
	/*	if ((status & 0xFF) != 5)
			PANIC();*/
		s++;	
	}
	SYSCALL(VERHOGEN, 0, 0, 0);
}

