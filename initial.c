#include "lib/const.h"
#include "lib/uMPStypes.h"
#include "lib/listx.h"
#include "lib/types11.h"

#include "lib/handlers.e"
/*Include Phase1 functions*/
#include "lib/pcb.e"
#include "lib/asl.e"
#include "lib/scheduler.e"
#include "lib/utils.e"
/*Include umps lib*/
#include "lib/libumps.e"

typedef struct{
	unsigned short int p;
	memaddr f;
}proc;

typedef struct{
	unsigned short int key;
	unsigned int mutex;
}semstr;


/*Inizia parte di inizializzazione di initial*/
/*Per far scattare un  trap non inizializzare proc_counter*/
unsigned int proc_counter=0, soft_block_counter=0, active_proc=0;
struct list_head ready_queue;
unsigned int mutex_rq = CFREE, mutex_semdh = CFREE, mutex_pcbh = CFREE, mutex_proc_c = CFREE, mutex_soft_counter= CFREE, mutex_active_proc = CFREE, mutex_store_status = CFREE;
/*Puntatori alle new/old areas per ogni cpu*/
state_t* areasPointers[MAX_CPU];
state_t pstates[MAX_CPU];
state_t new_old_areas[(MAX_CPU-1)][8];

/*5 linee di dispositivi più una per la gestione dei sub-device terminali, 8 dispositivi per ogni linea*/
unsigned int dev_semaphores_keys[6][8];
unsigned int sem_mutex[MAXSEM];
unsigned int run_mutex[MAX_CPU];
/*variabili temporanee di debug*/
int counters[MAX_CPU];
/*U32 test = 0;*/
U32 test2 = 0;
U32 test3 = 0;
int test5=0;
/*fine parte di inizializzazione di initial*/

void test();

/*funzioni associate ai processi di debug*/
void lazyProcess(){
	test2++;
	while(1);
}
void pocProcess(){
	test2++;
	/*SYSCALL(PASSEREN,3,0,0);*/
	myPrint("io sono proc");

/*	SYSCALL(TERMINATEPROCESS,0,0,0);*/
	SYSCALL(WAITCLOCK,0,0,0);
	test5=9;
	while(1);
}

void urgentProcess(){
	test2++;
/*	return;*/
/*	SYSCALL(TERMINATEPROCESS);*/
	/*SYSCALL(TERMINATEPROCESS, -1, 0, 0);*/
/*	SYSCALL(12, -1, 0, 0);*/
/*	test3 = SYSCALL(GETCPUTIME, 0, 0, 0);*/
/*	test3 = GET_TODLOW;*/
	/*if(test>10)
		SYSCALL(TERMINATEPROCESS,0,0,0);
	urgentProcess();	*/
	while(1);
}

void normalProcess(){
	int i;
	test2++;
	state_t child;
/*	test2= sizeof(pcb_t);
	STST(&child);
	child.status =  STATUS_INT_UNMASKED | STATUS_IEp | STATUS_IEc| STATUS_PLT_ENABLED;
	child.pc_epc = child.reg_t9 = (memaddr)urgentProcess;
	child.reg_sp=child.reg_sp-FRAME_SIZE;
	SYSCALL(CREATEPROCESS,(int)&child,5,0);
	for(i=0;i<100;i++);
	SYSCALL(TERMINATEPROCESS,0,0,0);*/
	SYSCALL(VERHOGEN,0,0,0);
	myPrint("Io sono normal");
	while(1);
}


/******************************************************************************
 * pKaya entry-point 
 ******************************************************************************/
void main() {
	int i,j,ram_sp=RAMTOP,count_key=CLOCK_TIMER;
	int bits=1,cpu_map=1,ptest=1;
	/*new_area[0] -> INT_NEWAREA, 1-> TLB_NEWAREA, 2->PGMTRAP_NEWAREA, 3->SYSBK_NEWAREA*/
/*	MAX_CPU= *(int *)0x10000500;*/
	pcb_t *test_process[MAXPROC];
/*	pcb_t *test_process;*/
	proc processi[4];
	/*Dal file const.h la struttura dell'array di new/old areas
	 * new_old_area[0] -> INT_OLDAREA
	 * new_old_area[1] -> INT_NEWAREA
	 * new_old_area[2] -> TLB_OLDAREA
	 * new_old_area[3] -> TLB_NEWAREA
	 * new_old_area[4] -> PGMTRAP_OLDAREA
	 * new_old_area[5] -> PGMTRAP_NEWAREA
	 * new_old_area[6] -> SYSBK_OLDAREA
	 * new_old_area[7] -> SYSBK_NEWAREA
	 * */
	
	/**(int*)(0x100003A0)=0x2;gli interrupt del terminale 0 vengono gestiti dalla cpu con id 2*/

	/**(int*)(0x100003A0)=0x1000000F; interrupt dinamici, tra le varie cpu*/
	for(i=0;i<MAX_CPU;i++){
		state_t* new_area[4];
		run_mutex[i]=CFREE;
		if(i==0){
			areasPointers[i]= (state_t*) INT_OLDAREA;
			new_area[0] = (state_t*) INT_NEWAREA;
			new_area[1] = (state_t*) TLB_NEWAREA;
			new_area[2] = (state_t*) PGMTRAP_NEWAREA;
			new_area[3] = (state_t*) SYSBK_NEWAREA;
		}
		else{	
			/*bits contiene sempre solo 1 bit a 1 nella posizione i*/
			bits=bits<<1;
			/*cpu_map mette insieme questi 1, per avere alla fine i bit di tutte le cpu*/
			cpu_map|=bits;
			areasPointers[i]=&new_old_areas[(i-1)][0];
			new_area[0] = &new_old_areas[(i-1)][1];
			new_area[1] = &new_old_areas[(i-1)][3];
			new_area[2] = &new_old_areas[(i-1)][5];
			new_area[3] = &new_old_areas[(i-1)][7];
		}
		/*I vari handler di varie cpu tra le cpu non devono condividere la stessa ram!*/
		/*inizializziamo l'handler degli interrupt*/
		new_area[0]->pc_epc=new_area[0]->reg_t9 = (memaddr)intHandler;
		new_area[0]->status =  STATUS_PLT_ENABLED;/*da rivedere*/
		new_area[0]->reg_sp=ram_sp=ram_sp-(OWN_PSIZE*4);

		/*inizializziamo l'handler delle eccezioni TLB*/
		new_area[1]->pc_epc=new_area[1]->reg_t9 = (memaddr)tlbHandler;
		new_area[1]->status =  STATUS_PLT_ENABLED;/*da rivedere*/
		new_area[1]->reg_sp=ram_sp=ram_sp-OWN_PSIZE;
		/*new_area[1]->reg_sp=RAMTOP-(OWN_PSIZE*(i*4+1));*/

		/*inizializziamo l'handler delle eccezioni program trap*/
		new_area[2]->pc_epc=new_area[2]->reg_t9 = (memaddr)trapHandler;
		new_area[2]->status =  STATUS_PLT_ENABLED;/*da rivedere*/
		new_area[2]->reg_sp=ram_sp=ram_sp-OWN_PSIZE;

		/*inizializziamo l'handler delle syscall*/
		new_area[3]->pc_epc=new_area[3]->reg_t9 = (memaddr)syscallHandler;
		new_area[3]->status =  STATUS_PLT_ENABLED;/*da rivedere*/
		new_area[3]->reg_sp=ram_sp=ram_sp-(OWN_PSIZE*6);

	}
	

	/*Inizia la configurazione delle Interrupt Rounting Entry*/
	/*Cpu_map contiene il rounting_entry che dobbiamo assegnare a tutti i devices installati*/
	cpu_map|=0x10000000; 
	/*Per tutte le linee di interrupt dei devices da 0-4*/
	for(i=0;i<5;i++){
		int line_dev_map = *(((int*)INST_BITMAP_START)+i);
/*		test3 = line_dev_map;*/
		/*Per ogni device 0-7*/
		for(j=0,bits=1;j<8;j++,bits<<=1){
			if(line_dev_map & bits){
				int* routing_entry = (int*)INIT_INTER_ROUNTING;
				routing_entry+=i*8+j;
				test3 = (int)routing_entry;
				*routing_entry=cpu_map;		
			}
		}	
	}
	/*Fine configurazione delle Interrupt Rounting Entry*/

/*	test2 = (int)*(int*)(0x100003A0);*/
	/*Settiamo a 0 il contatore di processi e il contatore dei processi bloccati per I/O*/
	proc_counter=0;
	soft_block_counter = 0;
	initPcbs();
	initASL();

	/*semafori di sistema*/
	/*settiamo tutte le variabili mutex a free*/
	for(i=0;i<MAXSEM;i++)
		sem_mutex[i]=CFREE;
	/*assegniamo ad ogni dispositivo una chiave(un numero progressivo) iniziando dall'ultima chiave disponibile che inizialmente è la chiave dopo CLOCK_TIMER*/
	for(i=0;i<6;i++)
		for(j=0;j<8;j++){
			dev_semaphores_keys[i][j]=count_key+=1;	
		}


	mkEmptyProcQ(&ready_queue);
	if(!ptest){
		processi[0].f =  (memaddr)normalProcess;
		processi[1].f =  (memaddr)lazyProcess;
		processi[2].f =  (memaddr)lazyProcess;
		processi[3].f =  (memaddr)pocProcess;
		processi[0].p = 5;
		processi[1].p = 1;
		processi[2].p = 3;
		processi[3].p = 1;
		for(i=0;i<4;i++){
			test_process[i] = allocPcb();
			test_process[i]->p_s.pc_epc=test_process[i]->p_s.reg_t9 =processi[i].f;
			test_process[i]->p_s.status =  0 | STATUS_INT_UNMASKED | STATUS_IEp | STATUS_IEc| STATUS_PLT_ENABLED;
			test_process[i]->p_s.reg_sp=ram_sp-(OWN_PSIZE*(MAX_CPU+i*4));
			test_process[i]->priority=processi[i].p;	
			proc_counter++;

			insertProcQ(&ready_queue,test_process[i]);
		}	
	}else{
		test_process[0] = allocPcb();
		test_process[0]->p_s.pc_epc=test_process[0]->p_s.reg_t9 =(memaddr)test;
		test_process[0]->p_s.status =  0 | STATUS_INT_UNMASKED | STATUS_IEp | STATUS_IEc| STATUS_PLT_ENABLED;
		test_process[0]->p_s.reg_sp=ram_sp-((OWN_PSIZE*5)*(MAX_CPU*4));
		test_process[0]->priority=5;	
		proc_counter++;

		insertProcQ(&ready_queue,test_process[0]);
	}

	SET_IT(SCHED_PSEUDO_CLOCK); 
	/*carichiamo lo scheduler in tutte le cpu partendo da quella con id più grande*/
	for(i=(MAX_CPU-1);i>=0;i--){
		STST(&pstates[i]);
		pstates[i].reg_sp = ram_sp = ram_sp-(OWN_PSIZE*5);
		pstates[i].pc_epc = pstates[i].reg_t9 = (memaddr)scheduler;
		pstates[i].status = STATUS_PLT_ENABLED;

		if(i==0){
			ram_sp = ram_sp-(OWN_PSIZE*4);
		/*	test3 = pstates[i].reg_sp;*/
			LDST(&pstates[i]);
		}else
			INITCPU(i,&pstates[i],&new_old_areas[(i-1)]);
	}

}

