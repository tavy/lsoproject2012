ARCH_PREFIX = mipsel-linux-
CC = $(ARCH_PREFIX)gnu-gcc
LD = $(ARCH_PREFIX)gnu-ld

CFLAGS = -Wall

all : kernel.core.umps

kernel.core.umps : kernel
	umps2-elf2umps -k $<
	#dopo aver trasformato l'eseguibile lo eliminiamo insieme ai file oggetto
	rm -f *.o kernel
	mv kernel.*.umps bin

kernel : initial.o pcb.o asl.o exceptions.o interrupts.o utils.o scheduler.o p2test2012_v1.0.o
	$(LD) -T /usr/local/share/umps2/umpscore.ldscript /usr/local/lib/umps2/crtso.o initial.o pcb.o asl.o exceptions.o interrupts.o utils.o scheduler.o /usr/local/lib/umps2/libumps.o p2test2012_v1.0.o -o kernel


p2test2012_v1.0.o: p2test2012_v1.0.c lib/const.h lib/uMPStypes.h lib/listx.h lib/types11.h
	$(CC) $(CFLAGS) -c p2test2012_v1.0.c

initial.o : initial.c lib/handlers.e lib/pcb.e lib/asl.e lib/const.h lib/uMPStypes.h lib/listx.h lib/types11.h
	$(CC) $(CFLAGS) -c initial.c
	
pcb.o : pcb.c lib/const.h lib/listx.h lib/types11.h
	$(CC) $(CFLAGS) -c pcb.c

asl.o : asl.c lib/const.h lib/listx.h lib/types11.h
	$(CC) $(CFLAGS) -c asl.c

exceptions.o : exceptions.c lib/exceptions.e lib/const.h lib/listx.h lib/types11.h
	$(CC) $(CFLAGS) -c exceptions.c

interrupts.o : interrupts.c lib/interrupts.e lib/const.h lib/listx.h lib/types11.h
	$(CC) $(CFLAGS) -c interrupts.c

utils.o : utils.c lib/utils.e lib/const.h lib/listx.h lib/types11.h
	$(CC) $(CFLAGS) -c utils.c

scheduler.o : scheduler.c lib/scheduler.e lib/const.h lib/listx.h lib/types11.h
	$(CC) $(CFLAGS) -c scheduler.c



	
clean :
	rm -f bin/kernel.*.umps

#doc:
#	w3m ../doc/doc.html || lynx ../doc/doc.html
